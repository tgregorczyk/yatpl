# Yet Another Toy Programming Language

## Usage

- The code is written in Rust by the way so you first have to install it, see https://www.rust-lang.org/tools/install

- You can compile and install the code with the command

```
cargo install --path .
```

The executable will be in ~/.cargo/bin/ which you can add to your PATH for simplicity (in .bashrc: export PATH=/home/\$USER/.cargo/bin:\$PATH)

- you can then run any code with:

```
yatpl path_to_file
```

- if you don't want to install the code you can just run:

```
cargo run path_to_file
```

- An exemple code is in the file named ... code.

## Features

- Bind expression to variables. Variables' names must start with a letter and can contain any character except ,?()[]+-\*/&|;:>\<= and of course whitespaces.

```
x = 3 + 4;
```

Do no forget the semi-colon!

Numbers are 32 bytes signed integers or floats (i32 or f32). They support the following operators: +, -, \*, /, \*\* for power, >, >=, \<, \<=, ==, !=, +=, -=, \*=, /=. For floats (e.g. 3.0), you cannot use == and !=. Notice that 2 / 3 returns 0 and 2 / 3.0 returns 0.666667.

- Evaluate values and print them to stdout. This is the only way to evaluate a variable. Parenthese are optional, arguments are separated by commas.

```
eval expression1, expression2;
```

- Functions: (they can be recursive!)

```
fn facto(n) {
    if n == 0 {
        return 1;
    } else {
        return n * facto(n - 1);
    }
}

eval facto(0), facto(5);
```

When you call a function you can omit the parentheses, but be careful ... E.g. this works:
```
eval facto 0, facto 5;
```

- Booleans are supported: true / false. Operators on booleans are: and (&& works), or (|| works), not (! works) and the ternary operator (bool ? if_true_return_this : otherwise_return_that)

```
a = true;
b = a ? 5 : 1;
eval b;
```

- IF statements are lazy and update global variables via the ternary operator.
Nested loops are tricky, you must know that variables declared only in one IF scope are local. Variables declared right outside are global for this scope. So if we nest two IF statements, the innermost scope won't know the global variables of the program which are two scopes above. See the code example in the project on how to circumvent this.

```
if bool {
   do this
} else {
   do that
}
```

- While loops are still in development. They are not lazy as they evaluate their condition right away. This means that they can't be used in IF statements or in functions.

```
a = 0;
while a < 5 {
   a = a + 1;
}
eval a;
```

- Strings are supported but you can only use ==, != and + (for concatenation) on them as for now:

```
a = "Hello" == "Hello";
b = "Hello, " + " World!";
eval a, b;
```

- Comments start with two dashes -- and end with a newline \\n

- Some builtin functions. I made the (poor?) choice that these functions are stronger than + or - operators but weaker than \*, /, \*\*, ... So 
```
int 2 * 3 == int(2*3)
int 2 + 3 == int(2) + 3
```

Currently the functions are: int, float, string, to convert variables; exp, ln, cos, sin, for trigonometric functions; reverse, length, upper and lower for strings.
```
eval string int 2.0;
eval reverse "hello";
```

- Lists. They can hold any of the previous values, and they can be recursive. Some builtin function on them are: head, tail, reverse, length, sum, product. You can add any element to a list with the + operator, and you can duplicate a list with the * operator and an integer. Conversion functions also work.
```
x = [[2, 3]] + [4.0, "5"];
eval head x;
eval int tail x;
eval sum tail x;
eval x * 3;
```

You can access a certain (0 based) index as such: 
```
x[0]
```

- for loops:
```
list = [1, 2, 3];
x = 0;
for a in list {
   x += a;
   dbg x;
}
eval a;
```

You can also use the range operator .. , 1..5 is equivalent to [1, 2, 3, 4]

- dbg as seen above is a lazy print operator to make debugging easier. It will be added to the AST and will be triggered by an eval statement as everything else.

## Limitations (and TODOs...)

- builtin and user-defined functions aren't handled in the same way
- you cannot eval inside functions or IF statements (implement a debug function)
- while loops are fragile (see above)
- error messages could be clearer (todo: implement a grammatical parser for the lexer)
- there could be more tests
- write a formatter

## How the interpreter works

- the lexer uses the nom crate (which is the only dependency). It returns a list of tokens (called a Lexer).
- the parser reads this Lexer and consumes it. It fills a HashMap of global variables (containing regular variables as well as functions) and their respective Abstrat Syntax Tree (AST). The ASTs are created with a Pratt parser taken from https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html.
- Functions have their own HashMap (called signature) containing their respective ASTs.
- The code is lazy, ASTs are only being evaluated when a *eval* function is invoked.
