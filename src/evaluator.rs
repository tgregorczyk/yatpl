pub mod evaluator {
    use crate::lexer::lexer::{Builtins, Operator};
    use crate::parser::parser::{print_return, Node};
    use core::panic;
    use std::collections::HashMap;
    use std::fmt;

    #[derive(Debug, Clone, PartialEq)]
    pub enum ReturnType {
        FLOAT(f32),
        BOOL(bool),
        STRING(String),
        INT(i32),
        LIST(Vec<Node>),
    }

    impl fmt::Display for ReturnType {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
                ReturnType::FLOAT(i) => write!(f, "{}", i),
                ReturnType::INT(i) => write!(f, "{}", i),
                ReturnType::BOOL(i) => write!(f, "{}", i),
                ReturnType::STRING(i) => write!(f, "\"{}\"", i),
                ReturnType::LIST(_) => {
                    // for elem in i {
                    //     write!(f, "{}", elem.to_return().fmt(f))
                    // }
                    Ok(())
                }
            }
        }
    }

    impl ReturnType {
        pub fn negative(&self) -> Self {
            match self {
                ReturnType::FLOAT(f) => ReturnType::FLOAT(-f),
                ReturnType::INT(f) => ReturnType::INT(-f),
                ReturnType::BOOL(f) => ReturnType::BOOL(!f),
                _ => panic!("{:?} doesn't work with the negative operator", self),
            }
        }

        pub fn float(self, s: &mut HashMap<String, Node>) -> Self {
            match self {
                ReturnType::INT(f) => ReturnType::FLOAT(f as f32),
                ReturnType::FLOAT(f) => ReturnType::FLOAT(f),
                ReturnType::STRING(f) => ReturnType::FLOAT(f.parse::<f32>().unwrap()),
                ReturnType::LIST(l) => {
                    let mut new: Vec<Node> = vec![];
                    for elem in l {
                        let res = evaluate_node(&elem, s).unwrap().float(s);
                        new.push(Node::Value(res));
                    }
                    ReturnType::LIST(new)
                }
                _ => panic!("float doesn't work with {:?}", self),
            }
        }

        pub fn int(self, s: &mut HashMap<String, Node>) -> Self {
            match self {
                ReturnType::INT(f) => ReturnType::INT(f),
                ReturnType::FLOAT(f) => ReturnType::INT(f as i32),
                ReturnType::STRING(f) => ReturnType::INT(f.parse::<f32>().unwrap() as i32),
                ReturnType::LIST(l) => {
                    let mut new: Vec<Node> = vec![];
                    for elem in l {
                        let res = evaluate_node(&elem, s).unwrap().int(s);
                        new.push(Node::Value(res));
                    }
                    ReturnType::LIST(new)
                }
                _ => panic!("int doesn't work with {:?}", self),
            }
        }

        pub fn string(self, s: &mut HashMap<String, Node>) -> Self {
            match self {
                ReturnType::STRING(f) => ReturnType::STRING(f),
                ReturnType::FLOAT(f) => ReturnType::STRING(f.to_string()),
                ReturnType::INT(i) => ReturnType::STRING(i.to_string()),
                ReturnType::LIST(l) => {
                    let mut new: Vec<Node> = vec![];
                    for elem in l {
                        let res = evaluate_node(&elem, s).unwrap().string(s);
                        new.push(Node::Value(res));
                    }
                    ReturnType::LIST(new)
                }
                _ => panic!("string doesn't work with {:?}", self),
            }
        }

        pub fn exponential(self) -> Self {
            match self {
                ReturnType::FLOAT(f) => ReturnType::FLOAT(f.exp()),
                ReturnType::INT(f) => ReturnType::FLOAT((f as f32).exp()),
                _ => panic!("exp doesn't work with {:?}", self),
            }
        }

        pub fn logarithm(self) -> Self {
            match self {
                ReturnType::FLOAT(f) => ReturnType::FLOAT(f.ln()),
                ReturnType::INT(f) => ReturnType::FLOAT((f as f32).ln()),
                _ => panic!("ln doesn't work with {:?}", self),
            }
        }

        pub fn cosinus(self) -> Self {
            match self {
                ReturnType::FLOAT(f) => ReturnType::FLOAT(f.cos()),
                ReturnType::INT(f) => ReturnType::FLOAT((f as f32).cos()),
                _ => panic!("cos doesn't work with {:?}", self),
            }
        }

        pub fn sinus(self) -> Self {
            match self {
                ReturnType::FLOAT(f) => ReturnType::FLOAT(f.sin()),
                ReturnType::INT(f) => ReturnType::FLOAT((f as f32).sin()),
                _ => panic!("sin doesn't work with {:?}", self),
            }
        }

        pub fn reverse(self) -> Self {
            match self {
                ReturnType::STRING(f) => ReturnType::STRING(f.chars().rev().collect()),
                ReturnType::LIST(mut l) => {
                    l.reverse();
                    ReturnType::LIST(l)
                }
                _ => panic!("reverse doesn't work with {:?}", self),
            }
        }

        pub fn length(self) -> Self {
            match self {
                ReturnType::STRING(f) => ReturnType::INT(f.len() as i32),
                ReturnType::LIST(l) => ReturnType::INT(l.len() as i32),
                _ => panic!("length doesn't work with {:?}", self),
            }
        }

        pub fn head(&self, s: &mut HashMap<String, Node>) -> Self {
            match self {
                ReturnType::STRING(f) => ReturnType::STRING(f.chars().nth(0).unwrap().to_string()),
                ReturnType::LIST(l) => evaluate_node(&l[0], s).unwrap(),
                _ => panic!("head doesn't work with {:?}", self),
            }
        }

        pub fn tail(&self) -> Self {
            match self {
                ReturnType::STRING(f) => ReturnType::STRING(f.chars().skip(1).collect::<String>()),
                ReturnType::LIST(l) => ReturnType::LIST(l.clone().into_iter().skip(1).collect()),
                _ => panic!("tail doesn't work with {:?}", self),
            }
        }

        pub fn sum(&self, s: &mut HashMap<String, Node>) -> Self {
            match self {
                ReturnType::LIST(l) => {
                    let first = l.first().unwrap();
                    let mut new = evaluate_node(&first, s).unwrap();
                    for elem in 1..l.len() {
                        let e = l.iter().nth(elem).unwrap();
                        let res = evaluate_node(&e, s).unwrap();
                        new = new.add(&res);
                    }
                    new
                }
                _ => panic!("int doesn't work with {:?}", self),
            }
        }

        pub fn product(&self, s: &mut HashMap<String, Node>) -> Self {
            match self {
                ReturnType::LIST(l) => {
                    let mut new = 1.0;
                    for elem in l {
                        let ReturnType::FLOAT(res) = evaluate_node(&elem, s).unwrap().float(s)
                        else {
                            panic!("should not happen");
                        };
                        new = new * res;
                    }
                    ReturnType::FLOAT(new)
                }
                _ => panic!("int doesn't work with {:?}", self),
            }
        }

        pub fn upper(&self) -> Self {
            match self {
                ReturnType::STRING(f) => ReturnType::STRING(f.to_uppercase()),
                _ => panic!("upper doesn't work with {:?}", self),
            }
        }

        pub fn lower(&self) -> Self {
            match self {
                ReturnType::STRING(f) => ReturnType::STRING(f.to_lowercase()),
                _ => panic!("lower doesn't work with {:?}", self),
            }
        }

        pub fn gt(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(f1 > f2),
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 > &(*f2 as f32)),
                    _ => panic!("gt not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 > f2),
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(&(*f1 as f32) > f2),
                    _ => panic!("gt not compatible with {:?}", self),
                },
                _ => panic!("gt not compatible with {:?}", self),
            }
        }
        pub fn ge(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(f1 >= f2),
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 >= &(*f2 as f32)),
                    _ => panic!("ge not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 >= f2),
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(&(*f1 as f32) >= f2),
                    _ => panic!("ge not compatible with {:?}", self),
                },
                _ => panic!("ge not compatible with {:?}", self),
            }
        }
        pub fn lt(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(f1 < f2),
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 < &(*f2 as f32)),
                    _ => panic!("lt not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 < f2),
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(&(*f1 as f32) < f2),
                    _ => panic!("lt not compatible with {:?}", self),
                },
                _ => panic!("lt not compatible with {:?}", self),
            }
        }
        pub fn le(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(f1 <= f2),
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 <= &(*f2 as f32)),
                    _ => panic!("le not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::BOOL(f1 <= f2),
                    ReturnType::FLOAT(f2) => ReturnType::BOOL(&(*f1 as f32) <= f2),
                    _ => panic!("le not compatible with {:?}", self),
                },
                _ => panic!("le not compatible with {:?}", self),
            }
        }
        pub fn add(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1 + f2),
                    ReturnType::INT(f2) => ReturnType::FLOAT(f1 + f2.clone() as f32),
                    ReturnType::STRING(f2) => ReturnType::FLOAT(f1 + f2.parse::<f32>().unwrap()),
                    ReturnType::LIST(f2) => {
                        let mut f2 = f2.clone();
                        f2.reverse();
                        f2.push(Node::Value(ReturnType::FLOAT(f1.clone())));
                        f2.reverse();
                        ReturnType::LIST(f2)
                    }
                    _ => panic!("add not compatible with {:?} and {:?}", self, other),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::INT(f1 + f2),
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1.clone() as f32 + f2),
                    ReturnType::STRING(f2) => ReturnType::INT(f1 + f2.parse::<i32>().unwrap()),
                    ReturnType::LIST(f2) => {
                        let mut f2 = f2.clone();
                        f2.reverse();
                        f2.push(Node::Value(ReturnType::INT(f1.clone())));
                        f2.reverse();
                        ReturnType::LIST(f2)
                    }
                    _ => panic!("add not compatible with {:?} and {:?}", self, other),
                },
                ReturnType::BOOL(f1) => match other {
                    ReturnType::LIST(f2) => {
                        let mut f2 = f2.clone();
                        f2.reverse();
                        f2.push(Node::Value(ReturnType::BOOL(f1.clone())));
                        f2.reverse();
                        ReturnType::LIST(f2)
                    }
                    _ => panic!("add not compatible with {:?} and {:?}", self, other),
                },
                ReturnType::LIST(f1) => match other {
                    ReturnType::LIST(f2) => {
                        let mut f1 = f1.clone();
                        let mut f2 = f2.clone();
                        f1.append(&mut f2);
                        ReturnType::LIST(f1)
                    }
                    x => {
                        let mut f1 = f1.clone();
                        f1.push(Node::Value(x.clone()));
                        ReturnType::LIST(f1)
                    }
                },
                ReturnType::STRING(f1) => match other {
                    ReturnType::STRING(f2) => ReturnType::STRING(format!("{f1}{f2}")),
                    ReturnType::INT(f2) => ReturnType::INT(f2 + f1.parse::<i32>().unwrap()),
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f2 + f1.parse::<f32>().unwrap()),
                    ReturnType::LIST(f2) => {
                        let mut f2 = f2.clone();
                        f2.reverse();
                        f2.push(Node::Value(ReturnType::STRING(f1.clone())));
                        f2.reverse();
                        ReturnType::LIST(f2)
                    }
                    _ => panic!("add not compatible with {:?} and {:?}", self, other),
                },
            }
        }

        pub fn substract(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1 - f2),
                    ReturnType::INT(f2) => ReturnType::FLOAT(f1 - f2.clone() as f32),
                    _ => panic!("substract not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::INT(f1 - f2),
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1.clone() as f32 - f2),
                    _ => panic!("substract not compatible with {:?}", self),
                },
                _ => panic!("substract not compatible with {:?}", self),
            }
        }

        pub fn multiply(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1 * f2),
                    ReturnType::INT(f2) => ReturnType::FLOAT(f1 * f2.clone() as f32),
                    _ => panic!("multiply not compatible with {:?}", self),
                },
                ReturnType::LIST(l) => match other {
                    ReturnType::INT(i) => {
                        let original = ReturnType::LIST(l.clone());
                        let mut new = ReturnType::LIST(l.clone());
                        for _ in 1..i.clone() {
                            new = new.add(&original)
                        }
                        new
                    }
                    _ => panic!("multiply not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::INT(f1 * f2),
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1.clone() as f32 * f2),
                    ReturnType::LIST(l) => {
                        let original = ReturnType::LIST(l.clone());
                        let mut new = ReturnType::LIST(l.clone());
                        for _ in 1..f1.clone() {
                            new = new.add(&original)
                        }
                        new
                    }
                    _ => panic!("multiply not compatible with {:?}", self),
                },
                _ => panic!("multiply not compatible with {:?}", self),
            }
        }

        pub fn power(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1.powf(f2.clone())),
                    ReturnType::INT(f2) => ReturnType::FLOAT(f1.powi(f2.clone())),
                    _ => panic!("power not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::FLOAT((f1.clone() as f32).powi(f2.clone())),
                    ReturnType::FLOAT(f2) => {
                        ReturnType::FLOAT((f1.clone() as f32).powf(f2.clone()))
                    }
                    _ => panic!("power not compatible with {:?}", self),
                },
                _ => panic!("power not compatible with {:?}", self),
            }
        }
        pub fn divide(&self, other: &Self) -> Self {
            match self {
                ReturnType::FLOAT(f1) => match other {
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1 / f2),
                    ReturnType::INT(f2) => {
                        if f2 == &0 {
                            panic!("attemting to divide by 0")
                        } else {
                            ReturnType::FLOAT(f1 / f2.clone() as f32)
                        }
                    }
                    _ => panic!("divide not compatible with {:?}", self),
                },
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => {
                        if f2 == &0 {
                            panic!("attemting to divide by 0")
                        } else {
                            ReturnType::INT(f1 / f2)
                        }
                    }
                    ReturnType::FLOAT(f2) => ReturnType::FLOAT(f1.clone() as f32 / f2),
                    _ => panic!("divide not compatible with {:?}", self),
                },
                _ => panic!("divide not compatible with {:?}", self),
            }
        }
        pub fn range(&self, other: &Self) -> Self {
            match self {
                ReturnType::INT(start) => match other {
                    ReturnType::INT(stop) => {
                        let mut list: Vec<Node> = vec![];
                        let start = start.clone() as usize;
                        let stop = stop.clone() as usize;
                        for i in start..stop {
                            let node = Node::Value(ReturnType::INT(i as i32));
                            list.push(node);
                        }
                        ReturnType::LIST(list)
                    }
                    _ => panic!("power not compatible with {:?}", self),
                },
                _ => panic!("power not compatible with {:?}", self),
            }
        }
        pub fn equal(&self, other: &Self) -> Self {
            match self {
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::BOOL(f1.clone() == f2.clone()),

                    _ => panic!("and not compatible with {:?}", self),
                },
                ReturnType::STRING(f1) => match other {
                    ReturnType::STRING(f2) => ReturnType::BOOL(f1.clone().eq(f2)),

                    _ => panic!("and not compatible with {:?}", self),
                },
                _ => panic!("and not compatible with {:?}", self),
            }
        }
        pub fn different(&self, other: &Self) -> Self {
            match self {
                ReturnType::INT(f1) => match other {
                    ReturnType::INT(f2) => ReturnType::BOOL(f1.clone() != f2.clone()),

                    _ => panic!("and not compatible with {:?}", self),
                },
                ReturnType::STRING(f1) => match other {
                    ReturnType::STRING(f2) => ReturnType::BOOL(!f1.clone().eq(f2)),

                    _ => panic!("and not compatible with {:?}", self),
                },
                _ => panic!("and not compatible with {:?}", self),
            }
        }
        pub fn and(&self, other: &Self) -> Self {
            match self {
                ReturnType::BOOL(f1) => match other {
                    ReturnType::BOOL(f2) => ReturnType::BOOL(f1.clone() & f2.clone()),

                    _ => panic!("and not compatible with {:?}", self),
                },
                _ => panic!("and not compatible with {:?}", self),
            }
        }
        pub fn or(&self, other: &Self) -> Self {
            match self {
                ReturnType::BOOL(f1) => match other {
                    ReturnType::BOOL(f2) => ReturnType::BOOL(f1.clone() | f2.clone()),

                    _ => panic!("or not compatible with {:?}", self),
                },
                _ => panic!("or not compatible with {:?}", self),
            }
        }

        pub fn dbg(&self, name: String, s: &mut HashMap<String, Node>) {
            match self {
                ReturnType::LIST(x) => {
                    let len = x.len();
                    let mut count = 0;
                    print!("[");
                    for elem in x {
                        count += 1;
                        let new = <Node as Clone>::clone(&elem).to_return(s);
                        print_return(new, s);
                        if count != len {
                            print!(", ")
                        }
                    }
                    print!("]");
                }
                _ => print!("(dbg: {name} = {self}) "),
            }
        }

        pub fn get_nth(&self, n: &Self, s: &mut HashMap<String, Node>) -> Self {
            match self {
                ReturnType::LIST(l) => match n {
                    ReturnType::INT(nth) => {
                        let new = l[nth.clone() as usize].clone();
                        new.to_return(s)
                    }

                    _ => panic!("get nth arg not compatible with {:?}", self),
                },
                _ => panic!("get nth arg not compatible with {:?}", self),
            }
        }
        pub fn ternary(&self, if_true: &Node, if_false: &Node) -> Node {
            match self {
                ReturnType::BOOL(cond) => {
                    if cond.clone() {
                        if_true.clone()
                    } else {
                        if_false.clone()
                    }
                }
                _ => panic!("ternary not compatible with {:?}", self),
            }
        }
    }

    pub fn evaluate_node(
        node: &Node,
        s: &mut HashMap<String, Node>,
    ) -> Result<ReturnType, Box<dyn std::error::Error>> {
        match node {
            Node::Value(ReturnType::LIST(l)) => {
                let mut list: Vec<Node> = vec![];
                for elem in l {
                    list.push(Node::Value(evaluate_node(elem, s).unwrap()))
                }
                Ok(ReturnType::LIST(list))
            }
            Node::Value(f) => Ok(f.clone()),
            Node::Function(f) => {
                let mut new_args: HashMap<String, Node> = HashMap::new();
                for arg in f.arguments.keys() {
                    let node = f.arguments.get(arg).unwrap();
                    let returntype = evaluate_node(node, s).unwrap();
                    new_args.insert(arg.clone(), Node::Value(returntype));
                }
                let mut arguments = new_args;
                // not sure how all this works
                for function in s.keys() {
                    if !arguments.contains_key(function) {
                        arguments.insert(function.clone(), s.get(function).unwrap().clone());
                    }
                }
                evaluate_node(s.get(&f.name).unwrap(), &mut arguments)
            }
            Node::Variable(name) => {
                if s.keys().any(|x| x == name) {
                    let nod = s.get(name).unwrap().clone();
                    evaluate_node(&nod, s)
                } else {
                    panic!("{:?} was never defined", name);
                }
            }
            Node::Expr(expr) => match expr.operands.len() {
                1 => match expr.op {
                    Operator::PLUS => Ok(evaluate_node(&expr.operands[0], s)?),
                    Operator::MINUS => Ok((evaluate_node(&expr.operands[0], s)?).negative()),
                    Operator::NOT => Ok((evaluate_node(&expr.operands[0], s)?).negative()),
                    Operator::BUILTINS(Builtins::INT) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).int(s))
                    }
                    Operator::BUILTINS(Builtins::FLOAT) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).float(s))
                    }
                    Operator::BUILTINS(Builtins::STRING) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).string(s))
                    }
                    Operator::BUILTINS(Builtins::EXP) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).exponential())
                    }
                    Operator::BUILTINS(Builtins::LN) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).logarithm())
                    }
                    Operator::BUILTINS(Builtins::COS) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).cosinus())
                    }
                    Operator::BUILTINS(Builtins::SIN) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).sinus())
                    }
                    Operator::BUILTINS(Builtins::LEN) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).length())
                    }
                    Operator::BUILTINS(Builtins::REVERSE) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).reverse())
                    }
                    Operator::BUILTINS(Builtins::UPPER) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).upper())
                    }
                    Operator::BUILTINS(Builtins::LOWER) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).lower())
                    }
                    Operator::BUILTINS(Builtins::HEAD) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).head(s))
                    }
                    Operator::BUILTINS(Builtins::TAIL) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).tail())
                    }
                    Operator::BUILTINS(Builtins::SUM) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).sum(s))
                    }
                    Operator::BUILTINS(Builtins::PRODUCT) => {
                        Ok((evaluate_node(&expr.operands[0], s)?).product(s))
                    }
                    _ => panic!("bad op: {:?}", expr.op),
                },
                2 => match expr.op {
                    Operator::DEBUG => {
                        let value = evaluate_node(&expr.operands[1], s)?;
                        let name = match &expr.operands[0] {
                            Node::Value(ReturnType::STRING(str)) => str.clone(),
                            _ => panic!("shouldn't happen"),
                        };
                        value.dbg(name, s);
                        Ok(value)
                    }
                    Operator::PLUS => Ok((evaluate_node(&expr.operands[0], s)?)
                        .add(&evaluate_node(&expr.operands[1], s)?)),

                    Operator::MINUS => Ok((evaluate_node(&expr.operands[0], s)?)
                        .substract(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::MULTIPLY => Ok((evaluate_node(&expr.operands[0], s)?)
                        .multiply(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::POWER => Ok((evaluate_node(&expr.operands[0], s)?)
                        .power(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::DIVISION => Ok((evaluate_node(&expr.operands[0], s)?)
                        .divide(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::AND => Ok((evaluate_node(&expr.operands[0], s)?)
                        .and(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::OR => Ok((evaluate_node(&expr.operands[0], s)?)
                        .or(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::GT => Ok((evaluate_node(&expr.operands[0], s)?)
                        .gt(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::GE => Ok((evaluate_node(&expr.operands[0], s)?)
                        .ge(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::LT => Ok((evaluate_node(&expr.operands[0], s)?)
                        .lt(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::LE => Ok((evaluate_node(&expr.operands[0], s)?)
                        .le(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::EQUALITY => Ok((evaluate_node(&expr.operands[0], s)?)
                        .equal(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::NONEQUALITY => Ok((evaluate_node(&expr.operands[0], s)?)
                        .different(&evaluate_node(&expr.operands[1], s)?)),
                    Operator::LBRACKET => Ok((evaluate_node(&expr.operands[0], s)?)
                        .get_nth(&evaluate_node(&expr.operands[1], s)?, s)),
                    Operator::RANGE => Ok((evaluate_node(&expr.operands[0], s)?)
                        .range(&evaluate_node(&expr.operands[1], s)?)),
                    _ => panic!("bad op: {:?}", &expr.op),
                },
                3 => match &expr.op {
                    Operator::INTERROGATION => {
                        let new_node = (evaluate_node(&expr.operands[0], s)?)
                            .ternary(&expr.operands[1], &expr.operands[2]);
                        Ok(evaluate_node(&new_node, s)?)
                    }
                    _ => panic!("bad op: {:?}", &expr.op),
                },
                _ => panic!("bad op: {:?}", &expr.op),
            },
        }
    }
}
