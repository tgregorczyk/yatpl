pub mod formatter {
    use crate::evaluator::evaluator::ReturnType;
    use crate::lexer::lexer::{Builtins, Lexer, Operator, Token};

    // TODO: wrong else ident
    pub fn format(mut lexer: Lexer) {
        let mut code = String::new();
        let tab = String::from("   ");
        let mut ident_level = 0;
        let mut previous_needs_tab = false;
        while lexer != Lexer::new() {
            let next = lexer.next();
            if next == Token::LBRACE {
                ident_level += 1;
            } else if next == Token::RBRACE {
                ident_level -= 1
            }
            let mut ident = String::new();
            for _ in 0..ident_level {
                ident = format!("{ident}{tab}")
            }
            let string = match_tok(next.clone(), lexer.peek());
            if previous_needs_tab {
                code = format!("{code}{ident}{string}")
            } else {
                code = format!("{code}{string}")
            }
            previous_needs_tab = false;
            if (next == Token::SEMICOLON) | (next == Token::LBRACE) {
                previous_needs_tab = true;
            }
        }
        println!("{code}");
    }

    // TODO: no space if following is RPAREN
    // TODO: no space if following is SEMICOLON for RPAREN
    // TODO: comments and blank lines ...
    fn match_tok(tok: Token, follower: Token) -> String {
        let whitespace = String::from(" ");
        let cond = (follower == Token::SEMICOLON)
            | (follower == Token::OPERATOR(Operator::POWER))
            | (follower == Token::OPERATOR(Operator::RPAREN))
            | (follower == Token::OPERATOR(Operator::RBRACKET))
            | (follower == Token::OPERATOR(Operator::RANGE))
            | (follower == Token::COMMA)
            | (follower == Token::OPERATOR(Operator::RPAREN));

        match tok {
            Token::FN => String::from("fn "),
            Token::SEMICOLON => String::from(";\n"),
            Token::COMMA => String::from(", "),
            Token::EQUAL => String::from("= "),
            Token::LBRACE => String::from("{\n"),
            Token::RBRACE => String::from("}\n"),
            Token::WHILE => String::from("while "),
            Token::IF => String::from("if "),
            Token::ELSE => String::from("else "),
            Token::FOR => String::from("for "),
            Token::IN => String::from("in "),
            Token::VARIABLE(str) => {
                if cond | (follower == Token::OPERATOR(Operator::LBRACKET)) {
                    str
                } else {
                    format!("{str}{whitespace}")
                }
            }
            Token::VALUE(x) => match x {
                ReturnType::STRING(i) => {
                    let str = i;
                    if cond {
                        str
                    } else {
                        format!("\"{str}\"{whitespace}")
                    }
                }
                _ => {
                    let str = x.to_string();
                    if cond {
                        str
                    } else {
                        format!("{str}{whitespace}")
                    }
                }
            },
            Token::EVAL => String::from("eval "),
            Token::OPERATOR(Operator::PLUS) => String::from("+ "),
            Token::OPERATOR(Operator::MINUS) => String::from("- "),
            Token::OPERATOR(Operator::MULTIPLY) => String::from("* "),
            Token::OPERATOR(Operator::DIVISION) => String::from("/ "),
            Token::OPERATOR(Operator::GT) => String::from("> "),
            Token::OPERATOR(Operator::LT) => String::from("< "),
            Token::OPERATOR(Operator::LE) => String::from("<= "),
            Token::OPERATOR(Operator::GE) => String::from(">= "),
            Token::OPERATOR(Operator::INTERROGATION) => String::from("? "),
            Token::OPERATOR(Operator::EQUALITY) => String::from("== "),
            Token::OPERATOR(Operator::NONEQUALITY) => String::from("!= "),
            Token::OPERATOR(Operator::AND) => String::from("&& "),
            Token::OPERATOR(Operator::OR) => String::from("|| "),
            Token::OPERATOR(Operator::LPAREN) => String::from("("),
            Token::OPERATOR(Operator::RPAREN) => {
                let str = String::from(")");
                if cond {
                    str
                } else {
                    format!("{str}{whitespace}")
                }
            }
            Token::OPERATOR(Operator::LBRACKET) => String::from("["),
            Token::OPERATOR(Operator::RBRACKET) => {
                let str = String::from("]");
                if cond {
                    str
                } else {
                    format!("{str}{whitespace}")
                }
            }
            Token::OPERATOR(Operator::COLON) => String::from(": "),
            Token::OPERATOR(Operator::RANGE) => String::from(".."),
            Token::OPERATOR(Operator::POWER) => String::from("**"),
            Token::OPERATOR(Operator::DEBUG) => String::from("dbg "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::INT)) => String::from("int "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::FLOAT)) => String::from("float "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::STRING)) => String::from("string "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::EXP)) => String::from("exp "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::LN)) => String::from("ln "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::COS)) => String::from("cos "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::SIN)) => String::from("sin "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::REVERSE)) => String::from("reverse "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::LEN)) => String::from("length "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::UPPER)) => String::from("upper "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::LOWER)) => String::from("lower "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::HEAD)) => String::from("head "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::TAIL)) => String::from("tail "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::SUM)) => String::from("sum "),
            Token::OPERATOR(Operator::BUILTINS(Builtins::PRODUCT)) => String::from("product "),
            t => panic!("unknown token {:?}", t),
        }
    }
}
