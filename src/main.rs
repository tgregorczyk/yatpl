use std::collections::HashMap;
use std::env;
use std::fs;

use crate::formatter::formatter::format;
use crate::lexer::lexer::{next_token, Lexer};
use crate::parser::parser::{update_variables, Node};

pub mod evaluator;
pub mod formatter;
pub mod lexer;
pub mod parser;
pub mod tests;

fn main() {
    let args: Vec<String> = env::args().collect();

    let file: String;
    if args[1] == String::from("--format") {
        file = args[2].clone();
    } else {
        file = args[1].clone();
    }

    let contents = fs::read_to_string(file).expect("no file");

    let mut tokens = Lexer::new();

    for mut line in contents.lines() {
        while line != "" {
            line = next_token(line, &mut tokens).unwrap();
        }
    }

    tokens.reverse();

    if args[1] == String::from("--format") {
        format(tokens);
    } else {
        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }
    }
}
