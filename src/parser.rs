pub mod parser {
    use crate::evaluator::evaluator::{evaluate_node, ReturnType};
    use crate::lexer::lexer::{Function, Lexer, Operator, Token};
    use core::panic;

    use std::collections::HashMap;

    #[derive(Debug, PartialEq, Clone)]
    pub enum Node {
        Expr(Expression),
        Value(ReturnType),
        Variable(String),
        Function(Function),
    }

    impl Node {
        pub fn to_return(self, s: &mut HashMap<String, Node>) -> ReturnType {
            match self {
                Self::Value(x) => x,
                Self::Expr(expr) => evaluate_node(&Self::Expr(expr), s).unwrap(),
                _ => panic!("cannot convert {:?} to ReturnType", self),
            }
        }
    }
    #[derive(Debug, PartialEq, Clone)]
    pub struct Expression {
        pub op: Operator,
        pub operands: Vec<Node>,
    }

    pub fn update_variables(
        variables: &mut HashMap<String, Node>,
        lexer: &mut Lexer,
        signature: &mut HashMap<String, Vec<String>>,
    ) {
        let mut name = String::new();
        let mut node = Node::Value(ReturnType::FLOAT(1.0));
        if lexer.peek() == Token::EVAL {
            parse_eval(lexer, signature, variables)
        } else if lexer.peek() == Token::OPERATOR(Operator::DEBUG) {
            parse_dbg(lexer, variables)
        } else if lexer.peek() == Token::IF {
            parse_if(lexer, variables, signature)
        } else if lexer.peek() == Token::WHILE {
            parse_while(lexer, variables, signature)
        } else if lexer.peek() == Token::FOR {
            parse_for(lexer, variables, signature)
        } else {
            (name, node) = match lexer.peek() {
                Token::FN => parse_fun(lexer, signature).unwrap(),
                Token::VARIABLE(_) => parse_binding(lexer, variables, signature).unwrap(),
                _ => panic!("something's wrong with {:?}", lexer.peek()),
            };
        }
        node = update_node(node, &variables);
        variables.insert(name, node);
    }

    // if we find a print we parse it here;
    pub fn parse_eval(
        lexer: &mut Lexer,
        sign: &mut HashMap<String, Vec<String>>,
        variables: &mut HashMap<String, Node>,
    ) {
        // eval
        lexer.next();
        // LPAREN
        match lexer.peek() {
            Token::OPERATOR(Operator::LPAREN) => {
                lexer.next();
            }
            _ => (),
        }
        let mut lexer_print = Lexer::new();
        loop {
            match lexer.peek() {
                Token::OPERATOR(Operator::RPAREN) => {
                    lexer.next();
                    break;
                }
                Token::SEMICOLON => {
                    break;
                }
                _ => {
                    let mut break_on_comma = false;
                    let mut precedent_is_variable = false;
                    loop {
                        match lexer.peek() {
                            Token::SEMICOLON => break,
                            _ => match lexer.next() {
                                Token::OPERATOR(Operator::RPAREN) => {
                                    break;
                                }
                                Token::COMMA => {
                                    break_on_comma = true;
                                    break;
                                }
                                Token::OPERATOR(Operator::LBRACKET) => {
                                    // this means we have array[n]
                                    if precedent_is_variable {
                                        lexer_print.push(Token::OPERATOR(Operator::LBRACKET))
                                    } else {
                                        let tok = parse_list(lexer);
                                        lexer_print.push(tok)
                                    }
                                    precedent_is_variable = false;
                                }
                                Token::VARIABLE(name_var) => {
                                    precedent_is_variable = true;
                                    if sign.keys().any(|x| x == &name_var) {
                                        parse_inside_fn(
                                            name_var,
                                            lexer,
                                            &mut lexer_print,
                                            sign,
                                            variables,
                                        )
                                    } else {
                                        lexer_print.push(Token::VARIABLE(name_var));
                                    }
                                }
                                t => {
                                    precedent_is_variable = false;
                                    lexer_print.push(t)
                                }
                            },
                        }
                    }
                    lexer_print.reverse();
                    let print_node = pratt_parser(&mut lexer_print, 0);
                    let print_value = evaluate_node(&print_node, variables).unwrap();
                    print_return(print_value, variables);
                    if break_on_comma {
                        print!(" ")
                    }
                }
            }
        }
        //SEMICOLON
        match lexer.peek() {
            Token::SEMICOLON => {
                lexer.next();
            }
            _ => (),
        }
        println!("");
    }

    // if we find a print we parse it here;
    pub fn parse_dbg(lexer: &mut Lexer, variables: &mut HashMap<String, Node>) {
        // dbg
        lexer.next();
        // LPAREN
        match lexer.peek() {
            Token::OPERATOR(Operator::LPAREN) => {
                lexer.next();
            }
            _ => (),
        }
        let name_var = match lexer.next() {
            Token::VARIABLE(var) => var,
            _ => panic!("we can only a variable name"),
        };
        // RPAREN
        match lexer.peek() {
            Token::OPERATOR(Operator::LPAREN) => {
                lexer.next();
            }
            _ => (),
        }
        let previous_node = variables.get(&name_var).unwrap();
        variables.insert(
            name_var.clone(),
            Node::Expr(Expression {
                op: Operator::DEBUG,
                operands: vec![
                    Node::Value(ReturnType::STRING(name_var)),
                    previous_node.clone(),
                ],
            }),
        );
        // SEMICOLON
        lexer.next();
    }

    pub fn print_return(l: ReturnType, s: &mut HashMap<String, Node>) {
        match l {
            ReturnType::LIST(x) => {
                let len = x.len();
                let mut count = 0;
                print!("[");
                for elem in x {
                    count += 1;
                    let new = elem.to_return(s);
                    print_return(new, s);
                    if count != len {
                        print!(", ")
                    }
                }
                print!("]");
            }
            _ => print!("{l}"),
        }
    }

    // if we find a function we parse it here;
    pub fn parse_fun(
        lexer: &mut Lexer,
        sign: &mut HashMap<String, Vec<String>>,
    ) -> Result<(String, Node), Box<dyn std::error::Error>> {
        // we pop the FN
        lexer.next();
        // this should be the name of the function
        let fn_name = lexer.next().extract_variable()?;
        // this should be a LPAREN
        lexer.next();
        let mut args: Vec<String> = vec![];
        loop {
            match lexer.next() {
                // we found an argument name
                Token::VARIABLE(var) => {
                    args.push(var);
                }
                Token::COMMA => (),
                Token::OPERATOR(Operator::RPAREN) => break,
                _ => panic!("waiting for RPAREN"),
            }
        }
        let mut inner_variables: HashMap<String, Node> = HashMap::new();
        for arg in &args {
            inner_variables.insert(arg.clone(), Node::Variable(arg.clone()));
        }
        sign.insert(fn_name.clone(), args);
        // return should always be an inner variable
        inner_variables.insert(String::from("return"), Node::Value(ReturnType::FLOAT(0.)));
        // we remove the LBRACE
        lexer.next();
        loop {
            match lexer.peek() {
                Token::RBRACE => break,
                _ => update_variables(&mut inner_variables, lexer, sign),
            }
        }
        // this should be a RBRACE
        lexer.next();
        // we treat return as a normal variable, we compute
        // its AST and we give it back
        let return_var = String::from("return");
        Ok((fn_name, inner_variables.get(&return_var).unwrap().clone()))
    }

    pub fn parse_while(
        lexer: &mut Lexer,
        variables: &mut HashMap<String, Node>,
        sign: &mut HashMap<String, Vec<String>>,
    ) {
        // we pop the WHILE
        lexer.next();
        // parse the condition
        let mut lexer_cond = Lexer::new();
        loop {
            match lexer.next() {
                Token::LBRACE => break,
                t => lexer_cond.push(t),
            }
        }
        lexer_cond.reverse();
        let mut lexer_cond_clone = lexer_cond.clone();
        let node_cond = pratt_parser(&mut lexer_cond_clone, 0);
        // this is the condition
        let mut cond = match evaluate_node(&node_cond, variables).unwrap() {
            ReturnType::BOOL(b) => b,
            t => panic!("condition should be a boolean not {:?}", t),
        };
        let mut inner_lexer = Lexer::new();
        let mut nb_paren = 1;
        while nb_paren != 0 {
            inner_lexer.push(lexer.next());
            match lexer.peek() {
                Token::RBRACE => {
                    nb_paren = nb_paren - 1;
                }
                Token::LBRACE => {
                    nb_paren = nb_paren + 1;
                }
                _ => (),
            }
        }
        // RBRACE
        lexer.next();
        inner_lexer.reverse();
        while cond {
            let mut inner_lexer_clone = inner_lexer.clone();
            while inner_lexer_clone != Lexer::new() {
                update_variables(variables, &mut inner_lexer_clone, sign)
            }
            let mut lexer_cond_clone = lexer_cond.clone();
            let node_cond = pratt_parser(&mut lexer_cond_clone, 0);
            // updating the condition
            cond = match evaluate_node(&node_cond, variables).unwrap() {
                ReturnType::BOOL(b) => b,
                t => panic!("condition should be a boolean not {:?}", t),
            };
        }
    }

    pub fn parse_for(
        lexer: &mut Lexer,
        variables: &mut HashMap<String, Node>,
        sign: &mut HashMap<String, Vec<String>>,
    ) {
        // we pop the FOR
        lexer.next();
        // this should be the name of the variable
        let for_var = lexer.next().extract_variable().unwrap();
        // we pop the IN
        lexer.next();
        // parse the list
        let mut lexer_list = Lexer::new();
        loop {
            match lexer.next() {
                Token::LBRACE => break,
                Token::OPERATOR(Operator::LBRACKET) => {
                    let tok = parse_list(lexer);
                    lexer_list.push(tok)
                }
                t => lexer_list.push(t),
            }
        }
        lexer_list.reverse();
        let list = match pratt_parser(&mut lexer_list, 0) {
            // not an ideal solution
            Node::Expr(expr) => evaluate_node(&Node::Expr(expr), variables).unwrap(),
            Node::Variable(var) => variables.get(&var).unwrap().clone().to_return(variables),
            Node::Value(ReturnType::LIST(l)) => ReturnType::LIST(l),
            _ => panic!("should not happen"),
        };
        let list = match list {
            ReturnType::LIST(l) => l,
            _ => panic!("should not happen"),
        };
        let mut inner_variables: HashMap<String, Node> = HashMap::new();
        let mut inner_lexer = Lexer::new();
        let mut nb_braces = 1;
        while nb_braces != 0 {
            let next = lexer.next();
            match next {
                Token::RBRACE => {
                    nb_braces = nb_braces - 1;
                }
                Token::LBRACE => {
                    nb_braces = nb_braces + 1;
                }
                _ => (),
            }
            inner_lexer.push(next);
        }
        // the last RBRACE
        inner_lexer.pop();
        inner_lexer.reverse();
        for elem in list {
            let mut lexer_clone = inner_lexer.clone();
            while lexer_clone != Lexer::new() {
                inner_variables.insert(for_var.clone(), elem.clone());
                update_variables(&mut inner_variables, &mut lexer_clone, sign);
            }
        }
        for inner_var in inner_variables.keys() {
            if inner_var != &for_var {
                if variables.contains_key(inner_var) {
                    let new =
                        update_node(inner_variables.get(inner_var).unwrap().clone(), variables);
                    variables.insert(inner_var.clone(), new);
                }
            }
        }
    }

    pub fn parse_if(
        lexer: &mut Lexer,
        variables: &mut HashMap<String, Node>,
        sign: &mut HashMap<String, Vec<String>>,
    ) {
        // we pop the IF
        lexer.next();
        // parse the condition
        let mut lexer_cond = Lexer::new();
        loop {
            match lexer.next() {
                Token::LBRACE => break,
                t => lexer_cond.push(t),
            }
        }
        lexer_cond.reverse();
        // this is the condition
        let node_cond = pratt_parser(&mut lexer_cond, 0);
        let mut if_variables: HashMap<String, Node> = HashMap::new();
        let mut else_variables: HashMap<String, Node> = HashMap::new();
        let mut nb_paren = 1;
        while nb_paren != 0 {
            update_variables(&mut if_variables, lexer, sign);
            match lexer.peek() {
                Token::RBRACE => {
                    nb_paren = nb_paren - 1;
                }
                Token::LBRACE => {
                    nb_paren = nb_paren + 1;
                }
                _ => (),
            }
        }
        // RBRACE
        lexer.next();
        match lexer.peek() {
            Token::ELSE => {
                // else
                lexer.next();
                // LBRACE
                lexer.next();
                let mut nb_paren = 1;
                while nb_paren != 0 {
                    update_variables(&mut else_variables, lexer, sign);
                    match lexer.peek() {
                        Token::RBRACE => {
                            nb_paren = nb_paren - 1;
                        }
                        Token::LBRACE => {
                            nb_paren = nb_paren + 1;
                        }
                        _ => (),
                    }
                }
                // RBRACE
                lexer.next();
                for if_var in if_variables.keys() {
                    if variables.contains_key(if_var) {
                        let if_node = if_variables.get(if_var).unwrap();
                        let if_node = update_local(if_node.clone(), variables, &if_variables);
                        let mut else_node = variables.get(if_var).unwrap();
                        if else_variables.contains_key(if_var) {
                            else_node = else_variables.get(if_var).unwrap()
                        }
                        let else_node = update_local(else_node.clone(), variables, &else_variables);
                        let new_node = update_node(
                            Node::Expr(Expression {
                                op: Operator::INTERROGATION,
                                operands: vec![
                                    node_cond.clone(),
                                    if_node.clone(),
                                    else_node.clone(),
                                ],
                            }),
                            variables,
                        );
                        variables.insert(if_var.clone(), new_node);
                    }
                }
                for else_var in else_variables.keys() {
                    if variables.contains_key(else_var) & !if_variables.contains_key(else_var) {
                        let if_node = variables.get(else_var).unwrap();
                        let else_node = else_variables.get(else_var).unwrap();
                        let else_node = update_local(else_node.clone(), variables, &else_variables);
                        let new_node = update_node(
                            Node::Expr(Expression {
                                op: Operator::INTERROGATION,
                                operands: vec![
                                    node_cond.clone(),
                                    if_node.clone(),
                                    else_node.clone(),
                                ],
                            }),
                            variables,
                        );
                        variables.insert(else_var.clone(), new_node);
                    }
                }
            }
            // if there is no else statement
            _ => {
                for if_var in if_variables.keys() {
                    if variables.contains_key(if_var) {
                        let if_node = if_variables.get(if_var).unwrap();
                        let if_node = update_local(if_node.clone(), variables, &if_variables);
                        let else_node = variables.get(if_var).unwrap();
                        let new_node = update_node(
                            Node::Expr(Expression {
                                op: Operator::INTERROGATION,
                                operands: vec![
                                    node_cond.clone(),
                                    if_node.clone(),
                                    else_node.clone(),
                                ],
                            }),
                            variables,
                        );
                        variables.insert(if_var.clone(), new_node);
                    }
                }
            }
        }
    }

    // if we find a binding (variable = expression;) we parse it here;
    pub fn parse_binding(
        lexer: &mut Lexer,
        variables: &mut HashMap<String, Node>,
        sign: &mut HashMap<String, Vec<String>>,
    ) -> Result<(String, Node), Box<dyn std::error::Error>> {
        // this should be the name of the variable
        let name = lexer.next().extract_variable()?;
        // this should be the EQUAL
        lexer.next();
        let mut lexer_expression = Lexer::new();
        let mut precedent_is_variable = false;
        loop {
            match lexer.next() {
                Token::SEMICOLON | Token::EOF => break,
                // parse array of the form [1, true, 3.0, "hello"]
                Token::OPERATOR(Operator::LBRACKET) => {
                    // this means we have array[n]
                    if precedent_is_variable {
                        lexer_expression.push(Token::OPERATOR(Operator::LBRACKET))
                    } else {
                        let tok = parse_list(lexer);
                        lexer_expression.push(tok)
                    }
                    precedent_is_variable = false;
                }
                Token::VARIABLE(name_var) => {
                    precedent_is_variable = true;
                    if sign.keys().any(|x| x == &name_var) {
                        parse_inside_fn(name_var, lexer, &mut lexer_expression, sign, variables)
                    } else {
                        lexer_expression.push(Token::VARIABLE(name_var));
                    }
                }
                t => {
                    lexer_expression.push(t);
                    precedent_is_variable = false;
                }
            }
        }
        lexer_expression.reverse();
        let node = pratt_parser(&mut lexer_expression, 0);
        let result = (name, node);
        Ok(result)
    }

    pub fn parse_list(lexer: &mut Lexer) -> Token {
        let mut nb_bracket = 1;
        let mut inner_lexer = Lexer::new();
        let mut list: Vec<Node> = vec![];
        while nb_bracket != 0 {
            loop {
                match lexer.next() {
                    Token::OPERATOR(Operator::RBRACKET) => {
                        nb_bracket = nb_bracket - 1;
                        if nb_bracket == 0 {
                            break;
                        }
                    }
                    Token::OPERATOR(Operator::LBRACKET) => {
                        let tok = parse_list(lexer);
                        inner_lexer.push(tok)
                    }
                    Token::COMMA => break,
                    t => inner_lexer.push(t),
                }
            }
            inner_lexer.reverse();
            let node = pratt_parser(&mut inner_lexer, 0);
            list.push(node);
        }
        Token::VALUE(ReturnType::LIST(list))
    }

    // takes a node and if it contains known variables we replace the Token::VARIABLE with its AST
    // found in s
    pub fn update_node(node: Node, s: &HashMap<String, Node>) -> Node {
        let new = match node {
            Node::Value(_) => node,
            Node::Function(_) => node,
            Node::Variable(var) => match s.get(&var) {
                Some(x) => x.clone(),
                None => Node::Variable(var),
            },
            Node::Expr(expression) => {
                let mut operands: Vec<Node> = vec![];
                for ope in expression.operands {
                    operands.push(update_node(ope.clone(), s))
                }
                Node::Expr(Expression {
                    op: expression.op,
                    operands,
                })
            }
        };

        new
    }

    pub fn update_local(
        node: Node,
        global: &HashMap<String, Node>,
        local: &HashMap<String, Node>,
    ) -> Node {
        let new = match node {
            Node::Value(_) => node,
            Node::Function(_) => node,
            Node::Variable(var) => {
                if local.contains_key(&var) & !global.contains_key(&var) {
                    local.get(&var).unwrap().clone()
                } else {
                    Node::Variable(var)
                }
            }
            Node::Expr(expression) => {
                let mut operands: Vec<Node> = vec![];
                for ope in expression.operands {
                    operands.push(update_local(ope.clone(), global, local))
                }
                Node::Expr(Expression {
                    op: expression.op,
                    operands,
                })
            }
        };

        new
    }

    // boilerplate code
    pub fn parse_inside_fn(
        name_var: String,
        lexer: &mut Lexer,
        inner_lexer: &mut Lexer,
        sign: &HashMap<String, Vec<String>>,
        variables: &mut HashMap<String, Node>,
    ) {
        // LPAREN
        match lexer.peek() {
            Token::OPERATOR(Operator::LPAREN) => {
                lexer.next();
            }
            _ => (),
        }
        let mut arguments: HashMap<String, Node> = HashMap::new();
        for arg in sign.get(&name_var).unwrap() {
            let mut lexer_args = Lexer::new();
            loop {
                match lexer.peek() {
                    Token::SEMICOLON => break,
                    _ => match lexer.next() {
                        Token::COMMA | Token::OPERATOR(Operator::RPAREN) => break,
                        t => lexer_args.push(t),
                    },
                }
            }
            lexer_args.reverse();
            let arg_node = pratt_parser(&mut lexer_args, 0);
            let arg_node = update_local(arg_node, &HashMap::new(), variables);
            arguments.insert(arg.clone(), arg_node);
        }
        inner_lexer.push(Token::FUNCTION(Function {
            name: name_var,
            arguments,
        }));
    }

    // Takes an expression represented by its Lexer (vector of tokens) and return the associated AST
    pub fn pratt_parser(lexer: &mut Lexer, min: u8) -> Node {
        let mut lhs = match lexer.next() {
            Token::VALUE(f) => Node::Value(f),
            Token::VARIABLE(f) => Node::Variable(f),
            Token::FUNCTION(f) => Node::Function(f),
            Token::OPERATOR(Operator::LPAREN) => {
                let lhs = pratt_parser(lexer, 0);
                assert_eq!(lexer.next(), Token::OPERATOR(Operator::RPAREN));
                lhs
            }
            Token::OPERATOR(op) => {
                let ((), r) = prefix_binding_power(&op);
                let rhs = pratt_parser(lexer, r);
                let operands: Vec<Node> = vec![rhs];
                Node::Expr(Expression { op, operands })
            }
            t => panic!("bad token {:?}", t),
        };

        loop {
            let op = match lexer.peek() {
                Token::EOF => break,
                Token::OPERATOR(op) => op,
                op => panic!("{:?} is not a supported operator", op),
            };

            if let Some((l, ())) = postfix_binding_power(&op) {
                if l < min {
                    break;
                }

                lexer.next();

                lhs = if op == Operator::LBRACKET {
                    let rhs = pratt_parser(lexer, 0);
                    assert_eq!(lexer.next(), Token::OPERATOR(Operator::RBRACKET));
                    Node::Expr(Expression {
                        op,
                        operands: vec![lhs, rhs],
                    })
                } else {
                    Node::Expr(Expression {
                        op,
                        operands: vec![lhs],
                    })
                };
                continue;
            }

            if let Some((l, r)) = infix_binding_power(&op) {
                if l < min {
                    break;
                }

                lexer.next();

                lhs = if op == Operator::INTERROGATION {
                    let mhs = pratt_parser(lexer, 0);
                    assert_eq!(lexer.next(), Token::OPERATOR(Operator::COLON));
                    let rhs = pratt_parser(lexer, r);
                    Node::Expr(Expression {
                        op,
                        operands: vec![lhs, mhs, rhs],
                    })
                } else {
                    let rhs = pratt_parser(lexer, r);
                    Node::Expr(Expression {
                        op,
                        operands: vec![lhs, rhs],
                    })
                };
                continue;
            }

            break;
        }
        lhs
    }

    fn prefix_binding_power(op: &Operator) -> ((), u8) {
        match op {
            Operator::BUILTINS(_) => ((), 9),
            Operator::PLUS | Operator::MINUS | Operator::NOT => ((), 15),
            _ => panic!("bad op: {:?}", op),
        }
    }

    fn postfix_binding_power(op: &Operator) -> Option<(u8, ())> {
        let res = match op {
            Operator::LBRACKET => (17, ()),
            _ => return None,
        };
        Some(res)
    }

    fn infix_binding_power(op: &Operator) -> Option<(u8, u8)> {
        let res = match op {
            Operator::GE
            | Operator::LE
            | Operator::GT
            | Operator::LT
            | Operator::EQUALITY
            | Operator::NONEQUALITY => (1, 2),
            Operator::INTERROGATION => (4, 3),
            Operator::RANGE => (5, 6),
            Operator::PLUS | Operator::MINUS | Operator::OR => (7, 8),
            Operator::MULTIPLY | Operator::DIVISION | Operator::AND => (11, 12),
            Operator::POWER => (13, 14),
            _ => return None,
        };
        Some(res)
    }
}
