pub mod lexer {
    use core::panic;
    use std::collections::HashMap;

    use crate::evaluator::evaluator::ReturnType;
    use crate::parser::parser::Node;

    use nom::{
        bytes::complete::tag,
        character::complete::{alpha0, char, digit1, i32, none_of, space0},
        combinator::opt,
        error::{Error, ErrorKind},
        multi::many0,
        IResult,
    };

    #[derive(Debug, PartialEq, Clone)]
    pub struct Lexer {
        pub tokens: Vec<Token>,
    }

    impl Lexer {
        pub fn new() -> Self {
            Lexer { tokens: vec![] }
        }
        pub fn push(&mut self, tok: Token) {
            self.tokens.push(tok);
        }
        pub fn pop(&mut self) {
            self.tokens.pop();
        }
        pub fn reverse(&mut self) {
            self.tokens.reverse();
        }
        pub fn next(&mut self) -> Token {
            self.tokens.pop().unwrap_or(Token::EOF)
        }
        pub fn peek(&mut self) -> Token {
            self.tokens.last().unwrap_or(&Token::EOF).clone()
        }
    }

    #[derive(Debug, PartialEq, Clone)]
    pub struct Function {
        pub name: String,
        pub arguments: HashMap<String, Node>,
    }

    #[derive(Debug, PartialEq, Clone)]
    pub enum Builtins {
        // int to float
        INT,
        FLOAT,
        STRING,
        EXP,
        LN,
        COS,
        SIN,
        REVERSE,
        LEN,
        UPPER,
        LOWER,
        HEAD,
        TAIL,
        SUM,
        PRODUCT,
    }

    // These are the possible operators
    // They have functions implemented for them and can be part of an AST and evaluated
    #[derive(Debug, PartialEq, Clone)]
    pub enum Operator {
        PLUS,
        MINUS,
        // greater / less than
        GT,
        LT,
        // greater / less or equal
        LE,
        GE,
        MULTIPLY,
        DIVISION,
        INTERROGATION,
        EQUALITY,
        NONEQUALITY,
        AND,
        OR,
        NOT,
        LPAREN,
        RPAREN,
        LBRACKET,
        RBRACKET,
        COLON,
        RANGE,
        POWER,
        DEBUG,
        BUILTINS(Builtins),
    }

    #[derive(Debug, PartialEq, Clone)]
    pub enum Token {
        FN,
        SEMICOLON,
        COMMA,
        EQUAL,
        LBRACE,
        RBRACE,
        WHILE,
        IF,
        ELSE,
        FOR,
        IN,
        OPERATOR(Operator),
        VARIABLE(String),
        VALUE(ReturnType),
        FUNCTION(Function),
        EVAL,
        EOF,
    }

    impl Token {
        pub fn extract_variable(&self) -> Result<String, Box<dyn std::error::Error>> {
            match self {
                Token::VARIABLE(string) => Ok(string.to_owned()),
                _ => Err(String::from("cannot extract variable").into()),
            }
        }
        pub fn extract_float(&self) -> Result<f32, Box<dyn std::error::Error>> {
            match self {
                Token::VALUE(ReturnType::FLOAT(float)) => Ok(float.clone()),
                _ => Err(String::from("cannot extract float").into()),
            }
        }
    }

    fn remove_whitespace(i: &str) -> IResult<&str, Option<&str>> {
        let (i, string) = opt(space0)(i)?;
        Ok((i, string))
    }

    fn parse_string(i: &str) -> IResult<&str, String> {
        let (i, _) = tag::<&str, &str, Error<&str>>("\"")(i)?;
        let (i, matched) = many0(none_of::<_, _, (&str, ErrorKind)>("\""))(i).unwrap();
        let (i, _) = tag::<&str, &str, Error<&str>>("\"")(i)?;
        let res = String::from_iter(matched);
        Ok((i, res))
    }

    fn parse_variable(i: &str) -> IResult<&str, String> {
        let (i, value) = alpha0::<&str, Error<&str>>(i)?;
        let (i, matched) =
            many0(none_of::<_, _, (&str, ErrorKind)>(" ,?[]()+-*/&|;:><="))(i).unwrap();
        let value = String::from(value);
        let matched = String::from_iter(matched);
        Ok((i, format!("{value}{matched}")))
    }

    fn parse_pluseq(i: &str) -> IResult<&str, String> {
        let (i, value) = parse_variable(i)?;
        let (i, _) = remove_whitespace(i)?;
        let (i, _) = tag::<&str, &str, Error<&str>>("+=")(i)?;
        Ok((i, value))
    }

    fn parse_minuseq(i: &str) -> IResult<&str, String> {
        let (i, value) = parse_variable(i)?;
        let (i, _) = remove_whitespace(i)?;
        let (i, _) = tag::<&str, &str, Error<&str>>("-=")(i)?;
        Ok((i, value))
    }

    fn parse_timeseq(i: &str) -> IResult<&str, String> {
        let (i, value) = parse_variable(i)?;
        let (i, _) = remove_whitespace(i)?;
        let (i, _) = tag::<&str, &str, Error<&str>>("*=")(i)?;
        Ok((i, value))
    }

    fn parse_diveq(i: &str) -> IResult<&str, String> {
        let (i, value) = parse_variable(i)?;
        let (i, _) = remove_whitespace(i)?;
        let (i, _) = tag::<&str, &str, Error<&str>>("/=")(i)?;
        Ok((i, value))
    }

    fn parse_float(i: &str) -> IResult<&str, String> {
        let (i, value1) = digit1::<&str, Error<&str>>(i)?;
        let (i, point) = tag::<&str, &str, Error<&str>>(".")(i)?;
        let (i, value2) = digit1::<&str, Error<&str>>(i)?;
        let float = format!("{value1}{point}{value2}");
        Ok((i, float))
    }

    fn parse_comment(i: &str) -> IResult<&str, &str> {
        let (i, comment) = tag::<&str, &str, Error<&str>>("--")(i)?;
        let (i, _) = many0(none_of::<_, _, (&str, ErrorKind)>("\n"))(i).unwrap();
        Ok((i, comment))
    }

    pub fn next_token<'a>(
        i: &'a str,
        tokens: &mut Lexer,
    ) -> Result<&'a str, Box<dyn std::error::Error>> {
        let Ok((i, _)) = remove_whitespace(i) else {
            panic!("problem in removing whitespaces")
        };
        if i == "" {
            return Ok("");
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("eval")(i) {
            tokens.push(Token::EVAL);
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("fn")(i) {
            tokens.push(Token::FN);
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("while")(i) {
            tokens.push(Token::WHILE);
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("if")(i) {
            tokens.push(Token::IF);
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("else")(i) {
            tokens.push(Token::ELSE);
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("true")(i) {
            tokens.push(Token::VALUE(ReturnType::BOOL(true)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("for")(i) {
            tokens.push(Token::FOR);
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("false")(i) {
            tokens.push(Token::VALUE(ReturnType::BOOL(false)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("and")(i) {
            tokens.push(Token::OPERATOR(Operator::AND));
            return Ok(string);
        }
        if let Ok((string, _)) = parse_comment(i) {
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("&&")(i) {
            tokens.push(Token::OPERATOR(Operator::AND));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("or")(i) {
            tokens.push(Token::OPERATOR(Operator::OR));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("||")(i) {
            tokens.push(Token::OPERATOR(Operator::OR));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("..")(i) {
            tokens.push(Token::OPERATOR(Operator::RANGE));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("return")(i) {
            tokens.push(Token::VARIABLE(String::from("return")));
            tokens.push(Token::EQUAL);
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("int")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::INT)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("float")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::FLOAT)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("string")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::STRING)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("exp")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::EXP)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("ln")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::LN)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("cos")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::COS)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("sin")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::SIN)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("reverse")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::REVERSE)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("length")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::LEN)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("upper")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::UPPER)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("lower")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::LOWER)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("head")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::HEAD)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("dbg")(i) {
            tokens.push(Token::OPERATOR(Operator::DEBUG));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("tail")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::TAIL)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("sum")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::SUM)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("product")(i) {
            tokens.push(Token::OPERATOR(Operator::BUILTINS(Builtins::PRODUCT)));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("==")(i) {
            tokens.push(Token::OPERATOR(Operator::EQUALITY));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("!=")(i) {
            tokens.push(Token::OPERATOR(Operator::NONEQUALITY));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("<=")(i) {
            tokens.push(Token::OPERATOR(Operator::LE));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>(">=")(i) {
            tokens.push(Token::OPERATOR(Operator::GE));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("not")(i) {
            tokens.push(Token::OPERATOR(Operator::NOT));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("**")(i) {
            tokens.push(Token::OPERATOR(Operator::POWER));
            return Ok(string);
        }
        if let Ok((string, _)) = tag::<&str, &str, Error<&str>>("in")(i) {
            tokens.push(Token::IN);
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('!')(i) {
            tokens.push(Token::OPERATOR(Operator::NOT));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('?')(i) {
            tokens.push(Token::OPERATOR(Operator::INTERROGATION));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('<')(i) {
            tokens.push(Token::OPERATOR(Operator::LT));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('>')(i) {
            tokens.push(Token::OPERATOR(Operator::GT));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('=')(i) {
            tokens.push(Token::EQUAL);
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('+')(i) {
            tokens.push(Token::OPERATOR(Operator::PLUS));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('-')(i) {
            tokens.push(Token::OPERATOR(Operator::MINUS));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('*')(i) {
            tokens.push(Token::OPERATOR(Operator::MULTIPLY));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('/')(i) {
            tokens.push(Token::OPERATOR(Operator::DIVISION));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>(':')(i) {
            tokens.push(Token::OPERATOR(Operator::COLON));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>(';')(i) {
            tokens.push(Token::SEMICOLON);
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>(',')(i) {
            tokens.push(Token::COMMA);
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('{')(i) {
            tokens.push(Token::LBRACE);
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('}')(i) {
            tokens.push(Token::RBRACE);
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('(')(i) {
            tokens.push(Token::OPERATOR(Operator::LPAREN));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>(')')(i) {
            tokens.push(Token::OPERATOR(Operator::RPAREN));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>('[')(i) {
            tokens.push(Token::OPERATOR(Operator::LBRACKET));
            return Ok(string);
        }
        if let Ok((string, _)) = char::<&str, Error<&str>>(']')(i) {
            tokens.push(Token::OPERATOR(Operator::RBRACKET));
            return Ok(string);
        }
        if let Ok((string, value)) = parse_float(i) {
            tokens.push(Token::VALUE(ReturnType::FLOAT(
                value.parse::<f32>().unwrap(),
            )));
            return Ok(string);
        }
        if let Ok((string, value)) = i32::<&str, Error<&str>>(i) {
            tokens.push(Token::VALUE(ReturnType::INT(value)));
            return Ok(string);
        }
        if let Ok((string, value)) = parse_pluseq(i) {
            tokens.push(Token::VARIABLE(String::from(value.clone())));
            tokens.push(Token::EQUAL);
            tokens.push(Token::VARIABLE(String::from(value)));
            tokens.push(Token::OPERATOR(Operator::PLUS));
            return Ok(string);
        }
        if let Ok((string, value)) = parse_minuseq(i) {
            tokens.push(Token::VARIABLE(String::from(value.clone())));
            tokens.push(Token::EQUAL);
            tokens.push(Token::VARIABLE(String::from(value)));
            tokens.push(Token::OPERATOR(Operator::MINUS));
            return Ok(string);
        }
        if let Ok((string, value)) = parse_timeseq(i) {
            tokens.push(Token::VARIABLE(String::from(value.clone())));
            tokens.push(Token::EQUAL);
            tokens.push(Token::VARIABLE(String::from(value)));
            tokens.push(Token::OPERATOR(Operator::MULTIPLY));
            return Ok(string);
        }
        if let Ok((string, value)) = parse_diveq(i) {
            tokens.push(Token::VARIABLE(String::from(value.clone())));
            tokens.push(Token::EQUAL);
            tokens.push(Token::VARIABLE(String::from(value)));
            tokens.push(Token::OPERATOR(Operator::DIVISION));
            return Ok(string);
        }
        if let Ok((string, value)) = parse_string(i) {
            tokens.push(Token::VALUE(ReturnType::STRING(String::from(value))));
            return Ok(string);
        }
        if let Ok((string, value)) = parse_variable(i) {
            if value == "" {
                let message = "token not found in ";
                let error = format!("{message}{string}");
                return Err(error.into());
            } else {
                tokens.push(Token::VARIABLE(String::from(value)));
                return Ok(string);
            }
        }
        let message = "token not found in ";
        let error = format!("{message}{i}");
        Err(error.into())
    }
}
