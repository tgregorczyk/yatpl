pub mod tests {
    #[test]
    fn test_next_token() {
        use crate::lexer::lexer::{next_token, Lexer};
        let mut tokens = Lexer::new();
        let mut string = "let fn (a, b) = if a {return true} else {return 1}";
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " fn (a, b) = if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " (a, b) = if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, "a, b) = if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, ", b) = if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " b) = if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, ") = if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " = if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " if a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " a {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " {return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, "return true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " true} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, "} else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " else {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " {return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, "return 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, " 1}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, "}");
        string = next_token(string, &mut tokens).unwrap();
        assert_eq!(string, "");
    }

    #[test]
    fn test_parse_let() {
        use crate::evaluator::evaluator::ReturnType;
        use crate::lexer::lexer::{Lexer, Operator, Token};
        use crate::parser::parser::{parse_binding, Expression, Node};
        use std::collections::HashMap;
        let mut lexer: Lexer = Lexer::new();
        lexer.push(Token::VARIABLE(String::from("x")));
        lexer.push(Token::EQUAL);
        lexer.push(Token::VALUE(ReturnType::FLOAT(2.0)));
        lexer.push(Token::OPERATOR(Operator::MULTIPLY));
        lexer.push(Token::VALUE(ReturnType::FLOAT(3.0)));
        lexer.push(Token::OPERATOR(Operator::PLUS));
        lexer.push(Token::VALUE(ReturnType::FLOAT(4.0)));
        lexer.push(Token::SEMICOLON);

        lexer.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut sign: HashMap<String, Vec<String>> = HashMap::new();
        while lexer != Lexer::new() {
            let (name, node) = match lexer.peek() {
                Token::VARIABLE(_) => parse_binding(&mut lexer, &mut variables, &mut sign).unwrap(),
                // Token::IF
                _ => panic!("something's wrong"),
            };
            variables.insert(name, node);
        }

        let operands_inner: Vec<Node> = vec![
            Node::Value(ReturnType::FLOAT(2.0)),
            Node::Value(ReturnType::FLOAT(3.0)),
        ];
        let operands: Vec<Node> = vec![
            Node::Expr(Expression {
                op: Operator::MULTIPLY,
                operands: operands_inner,
            }),
            Node::Value(ReturnType::FLOAT(4.0)),
        ];

        let (exp_name, exp_node) = (
            String::from("x"),
            Node::Expr(Expression {
                op: Operator::PLUS,
                operands,
            }),
        );

        let mut expected = HashMap::new();
        expected.insert(exp_name, exp_node);

        assert_eq!(lexer, Lexer::new());
        assert_eq!(variables, expected);
    }
    #[test]
    fn test_evaluate_node() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code = " x_#2 =  - 2 * (2+3); y = x_#2 + 1; fn function(var) { y = -3 + 4; return y - var; } z = -function(-x_#2); a = true; b = false; c = a and b; d = a or b and a; e = !a && b || a; f = a ? 3 : 1; g = !a ? 3 : 1;";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evalx = evaluate_node(variables.clone().get("x_#2").unwrap(), &mut variables).unwrap();
        let evaly = evaluate_node(variables.clone().get("y").unwrap(), &mut variables).unwrap();
        let evalz = evaluate_node(variables.clone().get("z").unwrap(), &mut variables).unwrap();
        let evalc = evaluate_node(variables.clone().get("c").unwrap(), &mut variables).unwrap();
        let evald = evaluate_node(variables.clone().get("d").unwrap(), &mut variables).unwrap();
        let evale = evaluate_node(variables.clone().get("e").unwrap(), &mut variables).unwrap();
        let evalt = evaluate_node(variables.clone().get("f").unwrap(), &mut variables).unwrap();
        let evalt2 = evaluate_node(variables.clone().get("g").unwrap(), &mut variables).unwrap();

        assert_eq!(evalx, ReturnType::INT(-10));
        assert_eq!(evaly, ReturnType::INT(-9));
        assert_eq!(evalz, ReturnType::INT(9));
        assert_eq!(evalc, ReturnType::BOOL(false));
        assert_eq!(evald, ReturnType::BOOL(true));
        assert_eq!(evale, ReturnType::BOOL(true));
        assert_eq!(evalt, ReturnType::INT(3));
        assert_eq!(evalt2, ReturnType::INT(1));
    }

    #[test]
    fn test_evaluate_if() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code = "a = 1; if a > 0 {b = 1; a = a + 1;} else {a = 1;}";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evala = evaluate_node(variables.clone().get("a").unwrap(), &mut variables).unwrap();

        assert_eq!(evala, ReturnType::INT(2));
    }

    #[test]
    fn test_recursive() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code = " fn facto(n) { if n > 0 { return n * facto(n - 1); } else { return 1; } } a = facto(0); b = facto(5); c = facto(12);";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evala = evaluate_node(variables.clone().get("a").unwrap(), &mut variables).unwrap();
        let evalb = evaluate_node(variables.clone().get("b").unwrap(), &mut variables).unwrap();
        let evalc = evaluate_node(variables.clone().get("c").unwrap(), &mut variables).unwrap();

        assert_eq!(evala, ReturnType::INT(1));
        assert_eq!(evalb, ReturnType::INT(120));
        assert_eq!(evalc, ReturnType::INT(479001600));
    }

    #[test]
    fn test_operators_variables() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code = "a = \"hello\" == \"hello\"; b = \"hello\" != \"hello\"; c = 2 / 3; d = 2 > 3.0; e = 3 / 2.0; f = 3 == 3; g = \"Hello,\" + \" World!\"; h = 2; h += 2; i = \"hi\"; i += \" you\"";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evala = evaluate_node(variables.clone().get("a").unwrap(), &mut variables).unwrap();
        let evalb = evaluate_node(variables.clone().get("b").unwrap(), &mut variables).unwrap();
        let evalc = evaluate_node(variables.clone().get("c").unwrap(), &mut variables).unwrap();
        let evald = evaluate_node(variables.clone().get("d").unwrap(), &mut variables).unwrap();
        let evale = evaluate_node(variables.clone().get("e").unwrap(), &mut variables).unwrap();
        let evalf = evaluate_node(variables.clone().get("f").unwrap(), &mut variables).unwrap();
        let evalg = evaluate_node(variables.clone().get("g").unwrap(), &mut variables).unwrap();
        let evalh = evaluate_node(variables.clone().get("h").unwrap(), &mut variables).unwrap();
        let evali = evaluate_node(variables.clone().get("i").unwrap(), &mut variables).unwrap();

        let hello = String::from("Hello, World!");
        let hi = String::from("hi you");

        assert_eq!(evala, ReturnType::BOOL(true));
        assert_eq!(evalb, ReturnType::BOOL(false));
        assert_eq!(evalc, ReturnType::INT(0));
        assert_eq!(evald, ReturnType::BOOL(false));
        assert_eq!(evale, ReturnType::FLOAT(1.5));
        assert_eq!(evalf, ReturnType::BOOL(true));
        assert_eq!(evalg, ReturnType::STRING(hello));
        assert_eq!(evalh, ReturnType::INT(4));
        assert_eq!(evali, ReturnType::STRING(hi));
    }

    #[test]
    fn test_function_from_another_function() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code = " fn g(a) { return 1; } fn h(b) { return 2; } fn facto(n) { if n > 0 { return g(n); } else { return h(n); } } a = facto(-12)";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evala = evaluate_node(variables.clone().get("a").unwrap(), &mut variables).unwrap();

        assert_eq!(evala, ReturnType::INT(2));
    }

    #[test]
    fn test_builtins() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code = "x = 23.5 ** 0.5; a = int x; c = int x + 2; d = int x * 2; b = string a; e = exp 0; f = cos 0.0;";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evala = evaluate_node(variables.clone().get("a").unwrap(), &mut variables).unwrap();
        let evalb = evaluate_node(variables.clone().get("b").unwrap(), &mut variables).unwrap();
        let evalc = evaluate_node(variables.clone().get("c").unwrap(), &mut variables).unwrap();
        let evald = evaluate_node(variables.clone().get("d").unwrap(), &mut variables).unwrap();
        let evale = evaluate_node(variables.clone().get("e").unwrap(), &mut variables).unwrap();
        let evalf = evaluate_node(variables.clone().get("f").unwrap(), &mut variables).unwrap();

        let d = String::from("4");
        assert_eq!(evala, ReturnType::INT(4));
        assert_eq!(evalb, ReturnType::STRING(d));
        assert_eq!(evalc, ReturnType::INT(6));
        assert_eq!(evald, ReturnType::INT(9));
        assert_eq!(evale, ReturnType::FLOAT(1.0));
        assert_eq!(evalf, ReturnType::FLOAT(1.0));
    }

    #[test]
    fn test_lists() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code =
            "x = [[2, 3], 4.0, \"5\"]; a = head x; b = int tail x; c = length x; d = reverse x; y = [2, 3.0, \"4\"]; e = sum y; f = product y; g = y[0];";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evala = evaluate_node(variables.clone().get("a").unwrap(), &mut variables).unwrap();
        let evalb = evaluate_node(variables.clone().get("b").unwrap(), &mut variables).unwrap();
        let evalc = evaluate_node(variables.clone().get("c").unwrap(), &mut variables).unwrap();
        let evald = evaluate_node(variables.clone().get("d").unwrap(), &mut variables).unwrap();
        let evale = evaluate_node(variables.clone().get("e").unwrap(), &mut variables).unwrap();
        let evalf = evaluate_node(variables.clone().get("f").unwrap(), &mut variables).unwrap();
        let evalg = evaluate_node(variables.clone().get("g").unwrap(), &mut variables).unwrap();

        assert_eq!(
            evala,
            ReturnType::LIST(vec![
                Node::Value(ReturnType::INT(2)),
                Node::Value(ReturnType::INT(3))
            ])
        );
        assert_eq!(
            evalb,
            ReturnType::LIST(vec![
                Node::Value(ReturnType::INT(4)),
                Node::Value(ReturnType::INT(5))
            ])
        );
        assert_eq!(evalc, ReturnType::INT(3),);
        assert_eq!(
            evald,
            ReturnType::LIST(vec![
                Node::Value(ReturnType::STRING(String::from("5"))),
                Node::Value(ReturnType::FLOAT(4.0)),
                Node::Value(ReturnType::LIST(vec![
                    Node::Value(ReturnType::INT(2)),
                    Node::Value(ReturnType::INT(3))
                ]))
            ])
        );
        assert_eq!(evale, ReturnType::FLOAT(9.0));
        assert_eq!(evalf, ReturnType::FLOAT(24.0));
        assert_eq!(evalg, ReturnType::INT(2));
    }

    #[test]
    fn test_for() {
        use crate::evaluator::evaluator::{evaluate_node, ReturnType};
        use crate::lexer::lexer::{next_token, Lexer};
        use crate::parser::parser::{update_variables, Node};
        use std::collections::HashMap;
        let code = "b = 1; for a in 1..4 {b *= a;} c = 0; list = 1..11; for a in list { c += a;}";
        let mut tokens = Lexer::new();

        for mut line in code.lines() {
            while line != "" {
                line = next_token(line, &mut tokens).unwrap();
            }
        }

        tokens.reverse();

        let mut variables: HashMap<String, Node> = HashMap::new();
        let mut function_signatures: HashMap<String, Vec<String>> = HashMap::new();
        while tokens != Lexer::new() {
            update_variables(&mut variables, &mut tokens, &mut function_signatures);
        }

        let evalb = evaluate_node(variables.clone().get("b").unwrap(), &mut variables).unwrap();
        let evalc = evaluate_node(variables.clone().get("c").unwrap(), &mut variables).unwrap();

        assert_eq!(evalb, ReturnType::INT(6));
        assert_eq!(evalc, ReturnType::INT(55));
    }
}
